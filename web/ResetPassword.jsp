
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> Reset Your Password - Online Book Store </title>
    </head>
    <body background=pics/blue.jpg>
        <form method="post" action="doReset">
            <font color="white">

            <br>
            <center>
                <%
                    String notMatched = (String) session.getAttribute("notMatched");
                    String emptyEntry = (String) session.getAttribute("emptyEntry");
                    if (notMatched != null && notMatched.equalsIgnoreCase("Yes")) {
                %>
                <h3> Not Matched </h3><br><br>
                <%
                    session.removeAttribute("notMatched");
                } else if (emptyEntry != null && emptyEntry.equalsIgnoreCase("Yes")) {
                %>
                <h3> Empty Entry </h3><br><br>
                <%
                    session.removeAttribute("emptyEntry");
                } else {
                %>
                <h3> Choose a new password </h3><br><br>
                <%
                    }
                %>

                <table>
                    <tr>
                        <td> Enter New Password: </td>
                        <td> <input type="password" name="newPass" value="">    (Case Sensitive) </td>
                    </tr>
                    <tr>
                        <td> Confirm New Password: </td>
                        <td> <input type="password" name="newPassConfirm" value="">    (Case Sensitive) </td>
                    </tr>
                    <tr><td>  </td></tr><tr></tr><tr></tr>
                    <tr>
                        <td>  </td>
                        <td> <input type="submit" value="Reset Now"> </td>
                    </tr>
                </table>
            </center>
            </font>
        </form>
    </body>
</html>
