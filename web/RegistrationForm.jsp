
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> Be A New User! </title>
    </head>
    <body background="pics/t3.jpg">
    <center>
        <form method="post" action="UserRegistration">
            <%
                String emptyEmail = (String) session.getAttribute("emptyEmail");
                String invalidEmail = (String) session.getAttribute("invalidEmail");
                if (emptyEmail != null && emptyEmail.equalsIgnoreCase("Yes")) {
            %>
            <h4><font color="brown"> You cannot leave email field empty </font></h4>
                <%
                    session.removeAttribute("emptyEmail");
                } else if (invalidEmail != null && invalidEmail.equalsIgnoreCase("Yes")) {
                %>
            <h4><font color="brown"> Invalid email, Enter a correct email </font></h4>
                <%
                    session.removeAttribute("invalidEmail");
                } else {
                %>
            <h3><font color="white"> Welcome To Registration page </font></h3>
                <%
                    }
                %>

            <br> <br>
            <table>
                <tr>
                    <td> Email Address </td>
                    <td> <input type="text" name="Email" value=""> <font color="red"> * </font> </td>                    
                </tr>
                <tr>
                    <td> </td>
                    <td> <input type="submit" value="Submit Now"> </td>
                </tr>
            </table>
        </form>
    </center>
</body>
</html>
