
<%@page import="org.apache.el.lang.ELSupport"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title> Login Page </title>
    </head>
    <body background="pics/login2.jpg">
        <form method="post" style="text-align: right">
            <a href="index.jsp"><font color="white"> Home </font> </a>
        </form>
        <form method="post" style="text-align: right">
            <a href="RegistrationForm.jsp"><font color="white"> Sign up</font> </a>
        </form>
    <center>
        <%
            int tryTimes = 0;
            String LoginSuccessfull = String.valueOf(session.getAttribute("LoginSuccessfull"));
            if (LoginSuccessfull.equalsIgnoreCase("null") || LoginSuccessfull.equalsIgnoreCase("Yes")) {
                session.removeAttribute("LoginSuccessfull");
        %>
        <h2> <font color="white"> Please Login To Your Account </font> </h2>
            <%
            } else if (LoginSuccessfull.equalsIgnoreCase("No")) {
                session.removeAttribute("LoginSuccessfull");
                if ((session.getAttribute("tryTimes")) == null) {
                    tryTimes = 0;
                } else {
                    tryTimes = Integer.parseInt(String.valueOf(session.getAttribute("tryTimes")));
                }
            %>
        <h2> <font color="white"> Password And Username Did Not Matched </font> </h2></center>
            <%
                }
            %>
    <br> <br> <br> <br> <center>
        <form action = "doLogin" method = "post"> <font color = "white" > UserName:</font >
            <input type = "text" name = "userName" value = ""> <br> <br>
            <font color = "white" > Password: </font >
            <input type = "password" name = "pass" value = "" > <br> <br>
            <input type = "submit" name = "submit" value = "Login" > 

            <% if (tryTimes >= 3) {
            %>
            <br><br><font color="white"> <a href="LoginHelp.jsp" style="color:white" > Need Help? </a></font>
            <%
                }
            %> 
        </form>
    </center>
</body>
</html>
