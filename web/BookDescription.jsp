<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <title> Book Description </title>
    </head>
    <body background="pics/t4.jpg">

        <%
            if (String.valueOf(session.getAttribute("LoginSuccessfull")).equalsIgnoreCase("Yes")) {
        %>
        <h4> 
            <form method="post" action="doLogOut" style="text-align: right;">
                <font style="color: snow"> welcome <%=session.getAttribute("name")%> </font>
                <a href="Home.jsp" style="color: burlywood"> Home </a>
                <button type="submit" class=""> LogOut </button>
            </form>
        </h4>
        <%
        } else {
        %>

        <h4><font style="color: burlywood"> YOU ARE NOT SIGN IN YET,</font> <a href="Login.jsp" style="color: snow"> LOGIN </a>
            <font style="color: burlywood"> OR </font><a href="RegistrationForm.jsp" style="color: snow">SIGN UP</a></h4>

        <%
            }
        %>
        <font color="white">
    <center> <h3> YOUR BOOK COMPLETE INFORMATION </h3> <br> 
        <%  String selectedISBN = request.getParameter("isbn");
            String url = "jdbc:mysql://localhost/bookstore";
            String query = "Select * from Books where isbn=?";

            try {
                Connection connection = DriverManager.getConnection(url, "root", "2323");
                PreparedStatement ps = connection.prepareStatement(query);
                ps.setString(1, selectedISBN);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
        %>
        <table border="2" cellspacing="10" cellpadding="20">
            <tr>
                <th> ISBN </th>
                <th> TITLE </th>
                <th> DESCRIBE </th>
                <th> AUTHOR </th>
                <th> PRICE($) </th>
                <th> <img src="pics/buy.png" height="50" width="50"/> </th>
            </tr>
            <tr>
                <td> <%=rs.getString("isbn")%> </td>
                <td> <%=rs.getString("title")%> </td>
                <td> <%=rs.getString("bookdescribe")%> </td>
                <td> <%=rs.getString("author")%> </td>
                <td> <%=rs.getString("price")%> </td>
                <td>  <a href="doBuy?isbn=<%= request.getParameter("isbn")%>"> Add To Card </a> </td>
            </tr>
        </table>

        <%
                }
            } catch (SQLException sqle) {
                sqle.printStackTrace();
            }
        %>
    </center>
</body>
</html>
