<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <title>Home Page</title>
    </head>
    <body background="pics/t4.jpg">

        <%
            String existsInDB = (String) session.getAttribute("LoginSuccessfull");
            if (existsInDB == null) {
        %>
        <font color="snow" style="text-align:left">
        You are not sign in yet, <a href="Login.jsp" style="color:snow " > Login </a>
        or <a href="RegistrationForm.jsp" style="color: snow"> Sign up </a>
        </font>
        <%
        } else if (existsInDB.equalsIgnoreCase("Yes")) {
            String name = (String) session.getAttribute("name");
        %>

        <form method="post" action="doLogOut" style="text-align: right">
            <font color="white"> Welcome <%= name%> </font>
            <a href = Checkout.jsp><font color="burlywood"> Checkout</font> </a>
            <button type="submit" class=""> LogOut </button>
        </form>

        <%
            }
            String buyAfterLoginDone = (String) session.getAttribute("buyAfterLoginDone");
            if (buyAfterLoginDone != null && buyAfterLoginDone.equalsIgnoreCase("Yes")) {
        %>
        <font color="white">Your previous order added to your account</font>
        <%
            }
        %>

    <center>

        <h4><font color="white"> Available Books In Book Store </font>  </h4>
        <form method="get" action="doBuy">
            <table border="2" cellspacing="10" cellpadding="20">
                <tr> 
                    <th> <font color="white"> ISBN </font> </th>
                    <th> <font color="white"> TITLE </font> </th>
                    <th> <font color="white"> PRICE </font> </th>
                </tr>

                <%
                    String url = "jdbc:mysql://localhost/bookstore";
                    String selectQuery = "Select * from briefBooks";
                    try {
                        Class.forName("com.mysql.jdbc.Driver");
                        Connection connection = DriverManager.getConnection(url, "root", "2323");
                        PreparedStatement ps = connection.prepareStatement(selectQuery);
                        ResultSet rs = ps.executeQuery();
                        while (rs.next()) {
                %>
                <tr>
                    <td><%=rs.getString("isbn")%> </td>
                    <td> <a href="BookDescription.jsp?isbn=<%= rs.getString("isbn")%>" style="color: snow"> <%= rs.getString("title")%> </a></td>
                    <td> <%= rs.getString("price")%> $ </td>
                </tr>
                <%
                        }
                    } catch (Exception ee) {
                        ee.printStackTrace();
                    }
                %>

            </table>
        </form>
    </center>
</body>
</html>
