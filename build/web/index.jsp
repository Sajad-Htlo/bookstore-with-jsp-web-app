<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> ONLINE BOOK STORE </title>
    </head>
    <body background="pics/t6.jpg">
        <a href="index.jsp"><h2><font color="white"> ONLINE BOOK STORE </font></h2></a>

        <form method="post" action="search" style="text-align:right">
            <font color ="white"> Search Book (by title)</font>
            <input type="text" name="searchKeyWord">
            <input type="submit" value="Search">
        </form>
        <%
            String emptySearch = (String) session.getAttribute("emptySearch");
            String noResult = (String) session.getAttribute("noResult");
            if (emptySearch != null && emptySearch.equalsIgnoreCase("Yes")) {
        %>
        <form style="text-align: right"><font color="white"> You should write something</font></form>
            <%
                session.removeAttribute("emptySearch");
            } else if (noResult != null && noResult.equalsIgnoreCase("Yes")) {
            %>
        <form style="text-align: right"><font color="white"> No result</font></form>
            <%
                session.removeAttribute("noResult");
                }

            %>
        <BR>
        <pre> <a href="index.jsp">  <font color="white"> Home </font> </a> </pre>
        <pre> <a href="Login.jsp"> <font color="white"> Login </font> </a> </pre>
        <pre> <a href="RegistrationForm.jsp"> <font color="white"> Registration </font> </a> </pre>
        <pre> <a href="About.jsp"> <font color="white"> About </font> </a> </pre>

        <form method="post" action="BookDescription">
            <center><h4> <font color="white">  Available Books </font> </h4>
                <table border="2" cellspacing="10" cellpadding="20">
                    <tr> 
                        <th> <font color="white"> ISBN </font> </th>
                        <th> <font color="white"> TITLE </font> </th>
                        <th> <font color="white"> PRICE </font> </th>
                    </tr>

                    <%  String url = "jdbc:mysql://localhost/bookstore";
                        String selectQuery = "Select * from briefBooks limit 5";
                        try {
                            Class.forName("com.mysql.jdbc.Driver");
                            Connection connection = DriverManager.getConnection(url, "root", "2323");
                            PreparedStatement ps = connection.prepareStatement(selectQuery);
                            ResultSet rs = ps.executeQuery();
                            while (rs.next()) {
                    %>
                    <tr>

                        <td> <font color="white"> <%=rs.getString("isbn")%> </font> </td>
                        <td> <a href="BookDescription.jsp?isbn=<%= rs.getString("isbn")%>" </a> <font color="00cccc"> <%=rs.getString("title")%> </font> </td>
                        <td> <font color="white"> <%=rs.getString("price")%> </font> </td>
                    </tr>
                    <%
                            }

                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                    %>

                </table>
            </center>
        </form>
    </body>
</html>
