<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Checkout </title>
    </head>
    <body background="pics/t1.jpg">
        <form method="post" action="doLogOut">
            <p style="text-align: right;">
                <button type="submit" class=""> LogOut </button>
            </p>
        </form>
    <center>
        <%
            if (session.getAttribute("isbn") != null) {
        %>

        <h3> <%=session.getAttribute("name")%>, Your checkout page </h3>
        <h2> Your Order(s) </h2>

        <table border="2" cellspacing="10" cellpadding="20">
            <tr>
                <th> Title </th>
                <th> Isbn </th>
                <th> Author </th>
                <th> Price </th>
            </tr>

            <%
                String url = "jdbc:mysql://localhost/bookstore";
                String createView = "create or replace view checkoutView as "
                        + "select * from books inner join buy using(isbn,price) where buy.username=?";
                String selectQuery = "select * from checkoutView";

                try {
                    Class.forName("com.mysql.jdbc.Driver");
                    Connection connection = DriverManager.getConnection(url, "root", "2323");
                    PreparedStatement ps1 = connection.prepareStatement(createView);
                    PreparedStatement ps2 = connection.prepareStatement(selectQuery);
                    ps1.setString(1, (String) session.getAttribute("username"));
                    ps1.execute();
                    ResultSet rs1 = ps2.executeQuery();
                    while (rs1.next()) {
            %>

            <tr>
                <td> <%=rs1.getString("title")%> </td>
                <td> <%=rs1.getString("isbn")%> </td>
                <td> <%=rs1.getString("author")%> </td>
                <td> <%=rs1.getString("price")%> </td>
            </tr>

            <%
                    }
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
            %>

        </table> <br>
        <%
            String totalPrice = "Select sum(price) from checkoutView";
            double totalCost = 0;
            try {
                Connection connection = DriverManager.getConnection(url, "root", "2323");
                PreparedStatement ps = connection.prepareStatement(totalPrice);
                ps.execute();
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    totalCost += Double.parseDouble(rs.getString("sum(price)"));
                }
            } catch (SQLException sqle) {
                sqle.printStackTrace();
            }
        %>
        Total Price = <%= totalCost%>

        <%
        } else {
        %>
        <h3> <font color="brown"> You have no selected item</font></h3>
        <a href="Home.jsp"> Back To Home </a>
        <%
            }
        %>
    </center>
</body>
</html>
