package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <img src=\"pics/Home-Headder.png\" > <br>\n");
      out.write("        <img src=\"pics/Links-Background.png\"> \n");
      out.write("\n");
      out.write("        <table>\n");
      out.write("            <tr> <a href=\"index.jsp\"> Home </a> </tr>\n");
      out.write("        <tr> <a href=\"Login.jsp\"> Login </a> </tr>\n");
      out.write("    <tr> <a href=\"RegistrationForm.jsp\"> Registration </a> </tr>\n");
      out.write("<tr> <a href=\"About.jsp\"> About us </a> </tr>\n");
      out.write("</table>\n");
      out.write("\n");
      out.write("<form method=\"post\" action=\"BookDescription\">\n");
      out.write("    <center><h4> Available Books </h4>\n");
      out.write("        <table border=\"2\" cellspacing=\"10\" cellpadding=\"20\">\n");
      out.write("            <tr> \n");
      out.write("                <th> ISBN </th>\n");
      out.write("                <th> TITLE </th>\n");
      out.write("                <th> PRICE </th>\n");
      out.write("            </tr>\n");
      out.write("\n");
      out.write("            ");
     String url = "jdbc:mysql://localhost/bookstore";
                String selectQuery = "Select * from briefBooks";
                try {
                    Class.forName("com.mysql.jdbc.Driver");
                    Connection connection = DriverManager.getConnection(url, "root", "2323");
                    PreparedStatement ps = connection.prepareStatement(selectQuery);
                    ResultSet rs = ps.executeQuery();
                    while (rs.next()) {
            
      out.write("\n");
      out.write("            <tr>\n");
      out.write("                <td> ");
      out.print(rs.getString("isbn"));
      out.write(" </td>\n");
      out.write("                <td> <a href=\"BookDescription.jsp?isbn= ");
      out.print( rs.getString("isbn"));
      out.write("\" </a>");
      out.print( rs.getString("title"));
      out.write(" </td>\n");
      out.write("                <td> ");
      out.print( rs.getString("price"));
      out.write(" </td>\n");
      out.write("            </tr>\n");
      out.write("            ");

                    }

                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            
      out.write("\n");
      out.write("\n");
      out.write("        </table>\n");
      out.write("    </center>\n");
      out.write("</form>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
