package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import org.apache.el.lang.ELSupport;
import javax.swing.JOptionPane;

public final class Login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <title> Login Page </title>\n");
      out.write("    </head>\n");
      out.write("    <body background=\"pics/login2.jpg\">\n");
      out.write("        <form method=\"post\" action=\"index.jsp\">\n");
      out.write("            <p style=\"text-align: right;\">\n");
      out.write("                <button type=\"submit\" class=\"\"> Home </button>\n");
      out.write("            </p>\n");
      out.write("        </form>\n");
      out.write("    <center>\n");
      out.write("        ");

            int tryTimes = 0;
            String LoginSuccessfull = String.valueOf(session.getAttribute("LoginSuccessfull"));
            JOptionPane.showMessageDialog(null, LoginSuccessfull);
            if (LoginSuccessfull.equalsIgnoreCase("null") || LoginSuccessfull.equalsIgnoreCase("Yes")) {
                session.removeAttribute("LoginSuccessfull");
        
      out.write("\n");
      out.write("        <h2> <font color=\"white\"> Please Login To Your Account </font> </h2>\n");
      out.write("            ");

            } else if (LoginSuccessfull.equalsIgnoreCase("No")) {
                session.removeAttribute("LoginSuccessfull");
                if ((session.getAttribute("tryTimes")) == null) {
                    tryTimes = 0;
                } else {
                    tryTimes = Integer.parseInt(String.valueOf(session.getAttribute("tryTimes")));
                    JOptionPane.showMessageDialog(null, tryTimes);
                }
            
      out.write("\n");
      out.write("        <h2> <font color=\"white\"> Password And Username Did Not Matched </font> </h2></center>\n");
      out.write("            ");

                }
            
      out.write("\n");
      out.write("    <br> <br> <br> <br> <center>\n");
      out.write("        <form action = \"doLogin\" method = \"post\"> <font color = \"white\" > UserName:</font >\n");
      out.write("            <input type = \"text\" name = \"userName\" value = \"\"> <br> <br>\n");
      out.write("            <font color = \"white\" > Password: </font >\n");
      out.write("            <input type = \"password\" name = \"pass\" value = \"\" > <br> <br>\n");
      out.write("            <input type = \"submit\" name = \"submit\" value = \"Login\" > \n");
      out.write("\n");
      out.write("            ");
 if (tryTimes >= 3) {
            
      out.write("\n");
      out.write("            <br><br><font color=\"white\"> <a href=\"LoginHelp.jsp\" style=\"color:white\" > Need Help? </a></font>\n");
      out.write("            ");

                }
            
      out.write(" \n");
      out.write("        </form>\n");
      out.write("    </center>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
