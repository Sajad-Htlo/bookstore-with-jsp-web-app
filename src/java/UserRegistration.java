
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/UserRegistration"})
public class UserRegistration extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title> Recieved data </title>");
        out.println("</head>");
        out.println("<body background=pics/t3.jpg>");
        out.println("<a href = Home.jsp><font color=white> Home </font> </a> ");
        out.println("<center><h3><font color=white>");
        response.setContentType("text/html;charset=UTF-8");
        String email = request.getParameter("Email").trim();
        if (email.length() != 0) {
            if (!valiEmail(email)) {
                request.getSession().setAttribute("invalidEmail", "Yes");
                response.sendRedirect("RegistrationForm.jsp");
            } else {
                if (sendActivationLink(email, email)) {
                    request.getSession().setAttribute("email", email);
                    out.println("An activation link has been sent to your email<br><br>");
                    out.println("Check Your Inbox");
                } else {
                    out.println("There is some error in sending activation email,");
                out.println("Check your network connection and <a href=RegistrationForm.jsp> Try again</a>");
                }
            }
        } else {
            request.getSession().setAttribute("emptyEmail", "Yes");
            response.sendRedirect("RegistrationForm.jsp");
        }
        out.println("</font></h3>");
        out.println("</center>");
        out.println("</body>");
        out.println("</html>");
    }

    private boolean sendActivationLink(String toEmail, String content) {
        final String username = "sajjad.htlo@gmail.com";
        final String password = "shahin45634563";
        final String htmlMessage = "Hello New User<br>"
                + "<h2> Here is your activation email on online book store</h2> <br><br>"
                + "<a href=http://localhost:8080/BookStore/RegistrationFormContinue.jsp> Activate Now </a>";

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("sajjad.htlo@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
            message.setSubject("Account Activation - Online Book Store");
            message.setContent(htmlMessage, "text/html; charset=utf-8");
            Transport.send(message);
            return true;
        } catch (MessagingException e) {
            return false;
        }
    }

    private boolean valiEmail(String email) {
        String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(EMAIL_REGEX);
    }
}
