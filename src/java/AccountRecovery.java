
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/AccountRecovery"})
public class AccountRecovery extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String typeOfProblem = (String) request.getSession().getAttribute("typeOfProb");
        out.println("<html>");
        out.println("<head>");
        out.println("<center>");
        out.println("<p style=text-align:right;");
        out.println("<font color=white> <a href = index.jsp style=color:white>Home</a></font></p><br><br>");
        if (typeOfProblem.equalsIgnoreCase("Password")) {
            String email = request.getParameter("emailForRecovery");
            String username = request.getParameter("usernameForRecovery");
            String result = getPasswordFromDB(username, email);
            if (result != null) {
                request.getSession().setAttribute("username", username);
                if (sendResetLinkToemail(email)) { // send successful
                    out.println("<title> Password Recovery Successfull </title>");
                    out.println("</head>");
                    out.println("<body background=pics/blue.jpg>");
                    out.println("<h3> <font color=white> Your account successfully recovered<br><br> </h3>");
                    out.println("Please check your inbox to reset your password</font>");
                } else {
                    out.println("<title>Password Sending Failed </title>");
                    out.println("</head>");
                    out.println("<body background=pics/blue.jpg>");
                    out.println("<h3> <font color=white> Your account successfully recovered<br><br>");
                    out.println("But There are some problems in sending to your email </h3></font>");
                }

            } else {
                out.println("<title>Password Recovery Failed</title>");
                out.println("</head>");
                out.println("<body background=pics/blue.jpg>");
                out.println("<h3> <font color=white> No account found with that email address </font></h3>");
                out.println("<a href=LoginHelp.jsp style=color:white> Try Again </a>");
            }
            out.println("</body>");
            out.println("</html>");
        } else if (typeOfProblem.equalsIgnoreCase("Username")) {
            String email = request.getParameter("emailForRecovery");
            String name = request.getParameter("nameForRecovery");
            String result = getUsernameFromDB(email, name);
            if (result != null) {
                if (SendUsernameToEmail(email, result)) {
                    out.println("<title>Username Recovery Successfull </title>");
                    out.println("</head>");
                    out.println("<body background=pics/blue.jpg>");
                    out.println("<h3><font color=white> Your account successfully recovered");
                    out.println("<h3><font color=white> Username has been sent to your email</font></h3>");
                } else {
                    out.println("<title>Username Sending Failed </title>");
                    out.println("</head>");
                    out.println("<body background=pics/blue.jpg>");
                    out.println("<h3><font color=white> Your account successfully recovered");
                    out.println("<h3><font color=white> But There are some problems in sending to your email </font></h3>");
                    out.println("<a href=LoginHelp.jsp style=color:white > Back To Account Recovery </a> ");
                }
            } else {
                out.println("<title>Username Recovery Failed </title>");
                out.println("</head>");
                out.println("<body background=pics/blue.jpg >");
                out.println("<br><h3><font color=white> No account found with that email address</font> </h3>");
                out.println("<a href=LoginHelp.jsp style=color:white > Back To Account Recovery </a> ");
            }
            out.println("</center>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    public String getPasswordFromDB(String username, String email) {
        String url = "jdbc:mysql://localhost/bookstore";
        String query = "Select password from users Where username=? and email=? ";
        try {
            Connection connection = DriverManager.getConnection(url, "root", "2323");
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString("password");
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
        return null;
    }

    public String getUsernameFromDB(String email, String name) {
        String url = "jdbc:mysql://localhost/bookstore";
        String query = "Select username from users Where email=? and name=? ";
        try {
            Connection connection = DriverManager.getConnection(url, "root", "2323");
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, email);
            ps.setString(2, name);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString("username");
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
        return null;
    }

    private boolean sendResetLinkToemail(String toEmail) {
        final String username = "sajjad.htlo@gmail.com";
        final String password = "shahin45634563";
        final String htmlMessage = "<form method=post action=http://localhost:8080/BookStore/ResetPassword.jsp>"
                + "<h2> Dear user </h2><br>"
                + " <h2> We've received your request to reset your password, and would be glad to help. </h2> "
                + "<title> Reset Your Password - Online Book Store </title><br>"
                + "Please click on button below to reset your password <br><br>"
                + "<input type=submit value=Reset Now> </form>";

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("sajjad.htlo@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
            message.setSubject("Reset Your Password - Online Book Store");
            message.setContent(htmlMessage, "text/html; charset=utf-8");

            Transport.send(message);
            return true;
        } catch (MessagingException e) {
            return false;
        }
    }

    private boolean SendUsernameToEmail(String toEmail, String content) {
        final String username = "sajjad.htlo@gmail.com";
        final String password = "shahin45634563";
        final String htmlMessage = "<h2> Dear user </h2><br>"
                + "Your Username is: " + content + "<br>"
                + "Please click on link below to login <br><br>"
                + "<a href=http://localhost:8080/BookStore/Login.jsp> Login Now </a>";

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("sajjad.htlo@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
            message.setSubject("Uername Recovery - Online Book Store");
            message.setContent(htmlMessage, "text/html; charset=utf-8");
            Transport.send(message);
            return true;
        } catch (MessagingException e) {
            return false;
        }
    }
}
