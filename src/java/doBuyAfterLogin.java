
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/doBuyAfterLogin"})
public class doBuyAfterLogin extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = (String) request.getSession().getAttribute("username");
        String isbn = (String) request.getSession().getAttribute("buyAfterLogin");
        doBuy doBuy1 = new doBuy();
        if (doBuy1.doBuyInDB(username, isbn, getDate(), getTime(), doBuy1.getPrice(isbn))) {
            request.getSession().setAttribute("buyAfterLoginDone", "Yes");
            response.sendRedirect("Home.jsp");
        }
    }

    private String getTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String currentTime = timeFormat.format(calendar.getTime());
        return currentTime;
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        String currentDate = dateFormat.format(date);
        return currentDate;
    }
}
