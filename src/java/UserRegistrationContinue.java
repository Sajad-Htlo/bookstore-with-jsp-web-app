
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/UserRegistrationContinue"})
public class UserRegistrationContinue extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet UserActivation</title>");
        out.println("</head>");
        out.println("<body background=pics/t3.jpg>");
        out.println("<center>");
        String name = request.getParameter("name").trim();
        String email = (String) request.getSession().getAttribute("email");
        String username = request.getParameter("username").trim();
        String definedPassword = request.getParameter("password").trim();
        String hashedInputPass = new String(hashPassword(definedPassword));

        if (!username.equals("") && !definedPassword.equals("")) {
            if (!duplicateUsername(username)) {
                if (insertNewUserInDB(username, hashedInputPass, email, name)) {
                    out.println("<h3><img src=pics/ok.png />Congratulation, You Sucessfully Registered!</a><br><br>");
                    out.println("<font color=white>You Can Sign in <a href = Login.jsp> Here </font> </h3> ");
                } else {
                    out.println("<img src= pics/error.png /> ");
                    out.println("<h2> There is some error in Registration");
                    out.println("<a href=RegistrationForm.jsp > Try Again </a> </h2>");
                }
            } else {
                out.println("<img src= pics/Error3.png />");
                out.println(" <h3> Duplicate username, Please Choose another username </h3> ");
                out.println("<a href=RegistrationFormContinue.jsp style=color:snow> Try Again </a> </h2>");
            }
        } else {
            out.println("<img src= pics/Error3.png />");
            out.println("<h3> invalie or empty data </h3> ");
            out.println("<a href=RegistrationFormContinue.jsp style=color:snow> Try Again </a> </h2>");
        }
        out.println("</center>");
        out.println("</body>");
        out.println("</html>");
    }

    private byte[] hashPassword(String password) {
        byte[] salt = new byte[16];
        byte[] hash = null;
        for (int i = 0; i < 16; i++) {
            salt[i] = (byte) i;
        }
        try {
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
            SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            hash = f.generateSecret(spec).getEncoded();
            System.out.println("salt in registration: " + new BigInteger(1, salt).toString(16));
            System.out.println("hash in registration: " + new BigInteger(1, hash).toString(16));

        } catch (NoSuchAlgorithmException nsale) {
            nsale.printStackTrace();

        } catch (InvalidKeySpecException ikse) {
            ikse.printStackTrace();
        }
        return hash;
    }

    private boolean insertNewUserInDB(String userN, String pass, String email, String name) {
        String url = "jdbc:mysql://localhost/bookstore";
        String query = "Insert into users values(?,?,?,?)";
        try {
            Connection connection = DriverManager.getConnection(url, "root", "2323");
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, userN);
            ps.setString(2, pass);
            ps.setString(3, email);
            ps.setString(4, name);
            if (ps.executeUpdate() == 1) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    private boolean duplicateUsername(String newUsername) {
        String url = "jdbc:mysql://localhost/bookstore";
        String query = "Select username from users where username=?";
        try {
            Connection connection = DriverManager.getConnection(url, "root", "2323");
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, newUsername);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (newUsername.equalsIgnoreCase(rs.getString("username"))) {
                    return true;
                }
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
        return false;
    }
}
