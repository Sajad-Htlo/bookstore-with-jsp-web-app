
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/search"})
public class search extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String keyword = request.getParameter("searchKeyWord").trim();
        if (keyword.length() == 0) {
            request.getSession().setAttribute("emptySearch", "Yes");
            response.sendRedirect("index.jsp");
        } else {
            Object[] result = searchInDB(keyword).toArray();
            String strResult = "";
            // create simple text of data
            for (int i = 0; i < result.length; i++) {
                strResult += result[i] + ",";
            }
            if (strResult.length() == 0) {
                request.getSession().setAttribute("noResult", "Yes");
                response.sendRedirect("index.jsp");
            } else {
                response.setContentType("text/html;charset=UTF-8");
                PrintWriter out = response.getWriter();
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet search</title>");
                out.println("</head>");
                out.println("<body background=pics/blue.jpg>");
                out.println("<center>");
                out.println("<font color =white> Search Book (by title)</font>");

                out.println("<form method=post action=index.jsp>");
                out.println("<p style=text-align:right;> <button type=submit > Home </button></p>");
                out.println("<h3><font color=white>search result for word: '" + keyword + "'</font></h3><br>");
                out.println("<table border=2 cellspacing=10 cellpadding=20>");
                out.println("<tr>");
                out.println("<th><font color=white>title</th></font>");
                out.println("<th><font color=white>isbn</th></font>");
                out.println("</tr>");

                String[] data = strResult.split(Pattern.quote(","));
                String[] titles = new String[data.length / 2];
                String[] isbnS = new String[data.length / 2];
                int count = 0;
                for (int i = 0; i < data.length; i += 2) {
                    titles[count] = data[i];
                    isbnS[count] = data[i + 1];
                    count++;
                }

                for (int i = 0; i < titles.length; i++) {
                    out.println("<tr>");
                    out.println("<td><font color=white>" + isbnS[i] + "</font></td>");
                    out.println("<td><a href=BookDescription.jsp?isbn=" + isbnS[i] + "><font color=00cccc>" + titles[i] + "</a></font></td>");
                    out.println("</tr>");
                }
                out.println("</font>");
                out.println("</table>");
                out.println("<center>");
                out.println("</body>");
                out.println("</html>");
            }
        }
    }

    private static List<String> searchInDB(String keyword) {
        List<String> results = new LinkedList<String>();
        String url = "jdbc:mysql://localhost/bookstore";
        String query = "Select title,isbn from books where title like ? ";
        try {
            Connection connection = DriverManager.getConnection(url, "root", "2323");
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, "%" + keyword + "%");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                results.add(rs.getString("title"));
                results.add(rs.getString("isbn"));
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();

        }
        return results;
    }
}
