
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/doBuy"})
public class doBuy extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = (String) request.getSession().getAttribute("username");
        String isbn = request.getParameter("isbn");
        if (username == null) {
            request.getSession().setAttribute("buyAfterLogin", isbn);
            response.sendRedirect("Login.jsp");
            return;
        }
        String url = "jdbc:mysql://localhost/bookstore";
        String selectQuery = "Select title,author from books inner join buy using(isbn,price) where username=? ";
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        String buyAfterLogin = (String) request.getSession().getAttribute("buyAfterLogin");
        boolean buyAfterLoginDone = false;
        if (buyAfterLogin != null) {
            if (doBuyInDB(username, buyAfterLogin, getDate(), getTime(), getPrice(buyAfterLogin))) {
                buyAfterLoginDone = true;
            }
        }
        if (doBuyInDB(username, isbn, getDate(), getTime(), getPrice(isbn))) {
            request.getSession().setAttribute("isbn", isbn);
            out.println("<title> Book added succesfully  </title>");
            out.println("</head>");
            out.println("<body background=pics/t4.jpg>");
            if (buyAfterLoginDone) {
                out.println("Your previous order added to your account");
            }
            out.println("<center>");
            out.println("<h3> <font color=white> Your Book Added to account successfully </font> </h3>");
            out.println("<a href = Home.jsp><font color=white> Add More Books</font> </a> ");
            out.println("<p>");
            out.println("<a href = Checkout.jsp><font color=white> Checkout</font></a> <br><br>");
            out.println("<font color=white> Your current order </font> ");
            out.println("<table border=2 cellspacing=10 cellpadding=20 >");
            out.println("<tr>");
            out.println("<th><font color=burlywood> TITLE </font></th>");
            out.println("<th><font color=burlywood> AUTHOR</font> </th>");
            out.println("</tr>");
            try {
                Connection connection = DriverManager.getConnection(url, "root", "2323");
                PreparedStatement ps = connection.prepareStatement(selectQuery);
                ps.setString(1, username);
                ps.execute();
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    out.println("<tr>");
                    out.println("<td>" + rs.getString("title") + " </td>");
                    out.println("<td>" + rs.getString("author") + " </td>");
                    out.println("</tr>");
                }
            } catch (SQLException sqle) {
                sqle.printStackTrace();
            }
            out.println("</table>");
        } else {
            out.println("<title>Error In purchase process</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h3> There is some problem during purchase process , Try again </h3>");
            out.println("<a href=Home.jsp> Try Again </a>");
        }
        out.println("</center>");
        out.println("</body>");
        out.println("</html>");
    }

    public boolean doBuyInDB(String username, String isbn, String date, String time, String price) {
        String url = "jdbc:mysql://localhost/bookstore";
        String query = "Insert into buy(username,isbn,date,time,price) values(?,?,?,?,?) ";
        try {
            Connection connection = DriverManager.getConnection(url, "root", "2323");
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, username);
            ps.setString(2, isbn);
            ps.setString(3, date);
            ps.setString(4, time);
            ps.setString(5, price);
            if (ps.executeUpdate() == 1) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    private String getTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String currentTime = timeFormat.format(calendar.getTime());
        return currentTime;
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        String currentDate = dateFormat.format(date);
        return currentDate;
    }

    public String getPrice(String isbn) {
        String url = "jdbc:mysql://localhost/bookstore";
        String query = "Select price from books where isbn=?";
        try {
            Connection connection = DriverManager.getConnection(url, "root", "2323");
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, isbn);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString("price");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
