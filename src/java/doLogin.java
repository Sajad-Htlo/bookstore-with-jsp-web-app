
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = {"/doLogin"})
public class doLogin extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userName = request.getParameter("userName");
        String inputPassword = request.getParameter("pass");
        String hashedInputPassword = new String(hashPassword(inputPassword));

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title> Servlet doLogin (in POST METHOD) </title>");
        out.println("</head>");
        out.println("<body>");
        if (checkInDatabase(userName, hashedInputPassword)) { // Exists
            HttpSession session = request.getSession(true); // create if on doesnt exists
            session.setAttribute("LoginSuccessfull", "Yes");
            session.removeAttribute("tryTimes");
            session.setAttribute("name", getName(userName));
            session.setAttribute("username", userName);
            if (request.getSession().getAttribute("buyAfterLogin") != null) {
                response.sendRedirect("doBuyAfterLogin");
            } else {
                response.sendRedirect("Home.jsp");
            }
        } else {
            HttpSession session = request.getSession(false);
            int i = 0;
            if (session.getAttribute("tryTimes") == null) {
                i = 0;
            } else {
                i = Integer.parseInt(String.valueOf(session.getAttribute("tryTimes")));
            }
            session.setAttribute("LoginSuccessfull", "No");
            session.setAttribute("tryTimes", ++i);
            response.sendRedirect("Login.jsp");
        }
        out.println("</body>");
        out.println("</html>");
    }

    public boolean checkInDatabase(String userN, String hashedInputPass) {
        String url = "jdbc:mysql://localhost/bookstore";
        String query = "Select * from users Where username=?";
        try {
            Connection connection = DriverManager.getConnection(url, "root", "2323");
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, userN);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (hashedInputPass.equalsIgnoreCase(rs.getString("password"))) {
                    return true;
                }
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            return false;
        }
        return false;
    }

    public String getName(String username) {
        String url = "jdbc:mysql://localhost/bookstore";
        String query = "Select name from users where username=?";
        try {
            Connection connection = DriverManager.getConnection(url, "root", "2323");
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, username);
            ps.execute();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString("name");
            }
            return null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            return null;
        }
    }

    private byte[] hashPassword(String password) {
        byte[] salt = new byte[16];
        byte[] hash = null;
        for (int i = 0; i < 16; i++) {
            salt[i] = (byte) i;
        }
        try {
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
            SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            hash = f.generateSecret(spec).getEncoded();
            System.out.println("salt is login: " + new BigInteger(1, salt).toString(16));
            System.out.println("hash in login: " + new BigInteger(1, hash).toString(16));

        } catch (NoSuchAlgorithmException nsale) {
            nsale.printStackTrace();

        } catch (InvalidKeySpecException ikse) {
            ikse.printStackTrace();
        }
        return hash;
    }
}
