
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/doReset"})
public class doReset extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet doReset (POST) </title>");
        out.println("</head>");
        out.println("<body background=pics/blue.jpg>");
        out.println("<center><font color=white>");
        String firstPass = request.getParameter("newPass").trim();
        String secondPass = request.getParameter("newPassConfirm").trim();
        if (firstPass.length() != 0 && secondPass.length() != 0) {
            if (firstPass.equals(secondPass)) { // new password accepted
                String username = (String) request.getSession().getAttribute("username");
                //hash new updated password
                String hashedUpdatedPass = new String(hashPassword(firstPass));
                if (updatePasswordInDB(username, hashedUpdatedPass)) {
                    out.println("<h3>Password successfully updated<br><bR>");
                    out.println("<a href = Login.jsp style=color:white> Login Here </a> </h3>");
                } else {
                    out.println("update in Database Failed");
                }
            } else {
                request.getSession().setAttribute("notMatched", "yes");
                response.sendRedirect("ResetPassword.jsp");
            }
        } else {
            request.getSession().setAttribute("emptyEntry", "Yes");
            response.sendRedirect("ResetPassword.jsp");
        }
        out.println("</center></font>");
        out.println("</body>");
        out.println("</html>");
    }

    private byte[] hashPassword(String password) {
        byte[] salt = new byte[16];
        byte[] hash = null;
        for (int i = 0; i < 16; i++) {
            salt[i] = (byte) i;
        }
        try {
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
            SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            hash = f.generateSecret(spec).getEncoded();
            System.out.println("salt in registration: " + new BigInteger(1, salt).toString(16));
            System.out.println("hash in registration: " + new BigInteger(1, hash).toString(16));

        } catch (NoSuchAlgorithmException nsale) {
            nsale.printStackTrace();

        } catch (InvalidKeySpecException ikse) {
            ikse.printStackTrace();
        }
        return hash;
    }

    private boolean updatePasswordInDB(String username, String newPass) {
        String url = "jdbc:mysql://localhost/bookstore";
        String query = "Update users set password=? where username=?";
        try {
            Connection connection = DriverManager.getConnection(url, "root", "2323");
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setString(1, newPass);
            ps.setString(2, username);
            if (ps.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            return false;
        }
        return false;
    }
}
