
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/doHelp"})
public class doHelp extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String typeOfProblem = request.getParameter("typeOfProblem");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        if (typeOfProblem.equalsIgnoreCase("password")) {
            request.getSession().setAttribute("typeOfProb", "password");
            out.println("<title>Password Recovery </title>");
            out.println("</head>");
            out.println("<body background=pics/blue.jpg>");
            out.println("<center>");
            out.print("<form style=text-align:left> <a href=LoginHelp.jsp style=color:white > Back </a> </form>");
            out.println("<font color=white><h3> Please Enter username and email</h3></font>");
            out.println("<br>");
            out.println("<table>");
            out.println("<form method=post action=AccountRecovery>");
            out.println("<tr>");
            out.println("<td><h4><font color=white> Username </font></h4></td>");
            out.println("<td><input type=text name=usernameForRecovery ></td> ");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td> <h4><font color=white> Email Address </font></h4> </td>");
            out.println("<td> <input type=text name=emailForRecovery > </td> ");
            out.println("</tr>");
            out.println("<tr> <td> </td> <td> </td> </tr>");
            out.println("<tr> <td> </td> <td> </td> </tr>");
            out.println("<tr>");
            out.println("<td> <input type=submit value=Continue ></td>");
            out.println("</tr>");
        } else {
            request.getSession().setAttribute("typeOfProb", "username");
            out.println("<title>Username Recovery </title>");
            out.println("</head>");
            out.println("<body background=pics/blue.jpg>");
            out.println("<center>");
            out.println("<font color=white><h3> Please Enter email and name</h3></font>");
            out.println("<br>");
            out.println("<form method=post action=AccountRecovery>");
            out.println("<table>");
            out.println("<tr>");
            out.println("<td><font color=white>Email Address</font></td>");
            out.println("<td><input type=text name=emailForRecovery></td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td><font color=white> Your name </font></td>");
            out.println("<td><input type=text name=nameForRecovery> </td>");
            out.println("</tr>");
            out.println("<tr>");
            out.println("<td><input type=submit value=Continue></td>");
            out.println("</tr>");
        }
        out.println("</form>");
        out.println("</table>");
        out.println("</center>");
        out.println("</body>");
        out.println("</html>");
    }
}
